// https://javascript.info/websocket
const ws = new WebSocket('ws://localhost:3000')

document.forms.publish.onsubmit = () => {
  ws.send(document.forms.publish.message.value)
  return false
}

// effectful dom manipulations
ws.onmessage = (event) => {
  const message = event.data

  let messageElement = document.createElement('div')
  messageElement.textContent = message
  document.getElementById('messages').prepend(messageElement)
}
