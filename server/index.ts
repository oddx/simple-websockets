const server = Bun.serve({
  fetch: (req, server) => server.upgrade(req) ?
    undefined :
    new Response('Upgrade failed :(', { status: 500 })
  ,
  websocket: {
    open: (ws) => {
      console.log('New connection')
      ws.subscribe('public')
    },
    message: (_, message) => {
      server.publish('public', `${message}`)
    },
    close: (ws) => {
      console.log('Connection closed')
      ws.unsubscribe('public')
    },
  },
})

console.log(`Listening on ${server.hostname}:${server.port}`);
