# simple-websockets
a simple websockets client and server

## running
To run:

```bash
nix develop
bun run server/index.ts
```
and to serve the client, open up a new terminal and open the html file with your browser

```bash
firefox client/index.html
```
